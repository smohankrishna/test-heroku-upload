package com.example.springdataintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringdataintegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringdataintegrationApplication.class, args);
	}
}
